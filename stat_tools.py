import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from os.path import join, dirname, realpath

def linear_regression (data):
    if isinstance(data, str):
        data = np.loadtxt(open(data, "rb"), delimiter=",", skiprows=1)

    x = data
    y = x[:, 0].copy()  # ! y = X[:,0] by ref not by val
    x[:, 0] = np.ones_like(y)
    xt = x.T
    xtx = xt.dot(x)
    xtx_inv = np.linalg.inv(xtx)
    b = xtx_inv.dot(xt).dot(y)

    if x.shape[1] == 2:
        fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(15, 6))
        plt.scatter(x[:, 1], y)
        plt.plot(x[:, 1], [b[0] + b[1] * a for a in x[:, 1]], color='r')
        fig.patch.set_facecolor('xkcd:mint green')


    return b


if __name__ == '__main__':
    csvf = 'boston_houses.csv'
    linear_regression(csvf)